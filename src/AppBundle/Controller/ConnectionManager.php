<?php
#namespace NotificationsBundle\Command;
namespace AppBundle\Controller;


use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ConnectionManager extends Controller implements  MessageComponentInterface
{

    protected $connections = array();
    
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * A new websocket connection
     *
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = $conn;
        $conn->send('..:: Hello from the Notification Center ::..');
        echo "New connection \n";
    }

    /**
     * Handle message sending
     *
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {   
        $messageData = json_decode(trim($msg));       
        echo $messageData->user->name;
        
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        foreach($this->connections as $key => $conn_element){
            if($conn === $conn_element){
                unset($this->connections[$key]);
                break;
            }
        }
    }

    /**
     * Error handling
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
    */ 
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->send("Error: " . $e->getMessage());
        echo "\n\n--------------------------------------------------------\n";
        echo ("Error: " . $e->getMessage());
        echo "\n--------------------------------------------------------\n\n";
        $conn->close();
    }
    


}

?>