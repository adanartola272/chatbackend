<?php

namespace AppBundle\Controller;


//use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\User; 

class DefaultController extends Controller
{
   
    private $session;
    /**
     * @Route("/", name="homepage")
     */
    public function createUser(Request $request)
    {
        $this->session = $request->getSession();
        $comment = "";
        if (!$this->session->has('userName')){
            $user = new User();        
            $form = $this->createUserForm($user);

            if ($request->getMethod() == 'POST'){
                $form->handleRequest($request);           

                if ($form->isValid() ){
                    $data = $form->getData();
                    if (!$this->userExist($data)){
                        $comment = "Fail";
                        
                    }else{
                        $comment = "Success, Welcome ".$data->getName();
                        $this->session->set("userName", $data->getName());

                        return $this-> render("default/account.html.twig",
                            array("comment"=>$comment
                            )
                        );
                    }
                }
                
            }

            return $this-> render("default/index.html.twig",
                        array( "form"=>$form->createView(),
                            "comment"=>$comment
                        )
            );
        }else{
            $comment = "Welcome ".$this->session->get("userName");
            return $this-> render("default/account.html.twig",
                        array("comment"=>$comment
                        )
            );
        } 

    }

    public function userExist($data)
    {
        $repo = $this->getDoctrine()->getRepository("AppBundle:User");

        $users = $repo->findOneBy(
            array('name' => $data->getName(),'password'=>$data->getPassword()
            )            
        );
            
        return $users;
        
    }

    public function createUserForm(User $user)
    {
        
        return $this->createFormBuilder($user)
            ->add('name',TextType::class)
            ->add("password",PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'Create User'))
            ->getForm();
    }

     /**
     * @Route("/create_user", name="createUser")
     */
    public function createAction()
    {
        $user = new User();
        $user->setName('user1');
        $user->setPassword('pass');
        $user->setLastLogin(new \DateTime());
        $user->setIsActive(True);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        
        return new Response('Created user id '.$user->getId());
    }

    /**
     * @Route("/logout", name="logoutUser")
     */
    public function logoutUser(Request $request)
    {
        $this->session = $request->getSession();
        $this->session->clear();
        return $this->redirectToRoute('homepage');
    }


}
