<?php
#namespace NotificationsBundle\Command;
namespace AppBundle\Controller;


use AppBundle\Controller\ConnectionManager;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServerCommand extends ContainerAwareCommand
{

    /**
     * Configure a new Command Line
     */
    protected function configure()
    {
        $this

            //->setName('Project:notification:server ') chat_backend:server
            ->setName('chat_backend:server')
            ->setDescription('Start the notification server.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $server = IoServer::factory(new HttpServer(
            new WsServer(
                new ConnectionManager($this->getContainer())
            )
        ), 5002);

        echo "Server Running!!\n";

        $output->writeln(
            'Will only be printed in verbose mode or higher',
            OutputInterface::VERBOSITY_VERBOSE
        );

        $server->run();

        

    }

}

?>